package tema5;

import java.io.IOException;
import java.nio.file.Files;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredData {
	

	private static final long ONE_HOUR = 60;
 
	 private String startTime;
	 private String endTime;
	 private String activity;
	 
	 
	 List<MonitoredData> data;
	 
	 public MonitoredData(String startTime,String endTime,String activity){
		 this.startTime=startTime;
		 this.endTime=endTime;
		 this.activity=activity;
	 }
	 
	 
	 
	 public void setStartTime(String start) {
		this.startTime = start;
	 }
	 
	 public void setEndTime(String end) {
		this.endTime = end;
	 }
	 
	 public void setActivity(String activity) {
		this.activity = activity;
	 }
	 
	 public String getStartTime(){
		 
		 return this.startTime;
	 }
	 
	 public String getEndTime() {
		return endTime;
	 }
	 
	 public String getActivity() {
		return activity;
	 }

	 
     
    
     
     
	 public void readData(){
		 
		 
		 List<String> list = new ArrayList<>();
		 
		 try (Stream<String> stream = Files.lines(Paths.get("Activities.txt"))) {

				list = stream.collect(Collectors.toList());
				
				list.stream().forEach((v)->{
		            String[] s=v.split("\t\t");
		            data.add(new MonitoredData(s[0],s[1],s[2]));
		            });
                 

			} catch (IOException e) {
				e.printStackTrace();
			}
	 }
	 
	 public long DistinctDays( ){
		 
		 List<String> counter = data.stream().map(MonitoredData::getStartTime).collect(Collectors.toList());
		 List<String> zile = counter.stream().map(s-> s.split(" ")).map(s-> new String(s[0])).collect(Collectors.toList());
		 
		 return zile.stream().distinct().count();
				 
	 }
	 
	 public void Activity(){
		 
		 	 List<String> activitate = data.stream().map(MonitoredData::getActivity).collect(Collectors.toList());
		 Map<String, Long> mapare = activitate.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
		 
		 try{
			
		 }
		
		 catch(IOException e){
			 e.printStackTrace();
		 }
	 }
	 

	
	 
	 public void maiMic5(){
			
			Map<String, List<Long>> rez;
						
		    rez = data.stream().collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.mapping(MonitoredData::getDurata, Collectors.toList())));
			
			List<String> list = new ArrayList<String>();
			
			rez.forEach( 
					(key,v)->{
	            int suma=0;
	            
	            for (Long valoare:v)
	                if (valoare<5) suma++;
	            if (suma/v.size()>=0.9){
	            	 list.add(key);
	            }
	            }
					);
			
			
			 catch(IOException e){
				 e.printStackTrace();
			 }
			
		 }
	 

	 }